import React, {Component} from 'react';
import Header from './header';
import Contents from './content';

class App extends Component {
    constructor() {
        super();
        this.state = {
            question:[],
            getQuestion: {}
        }

    }

    componentWillMount() {
        
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    shouldComponentUpdate(nextProps, nextState) {

    }

    componentWillUpdate(nextProps, nextState) {

    }

    componentDidUpdate(prevProps, prevState) {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div>
                <Header/>
                <Contents {...this.state}/>
            </div>
        );
    }
}

App.propTypes = {

};

export default App;

