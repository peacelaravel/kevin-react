import React, {Component} from 'react';

export default class Header extends Component {
    render() {
        let header  =   <header>
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-10 col-md-offset-1">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <div className="logo">
                                                    <a href="#/"><img src="assets/images/logo.png"/></a>
                                                </div>
                                            </div>
                                            <div className="col-sm-8">
                                                <div className="navigation">
                                                    <ul>
                                                        <li><a href="">quiz</a></li>
                                                        <li><a href="">about</a></li>
                                                        <li><a href="">blog</a></li>
                                                        <li><a href="#/submit" className="submit">submit</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header> ;
        return (header);
    }
}