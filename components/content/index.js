import React, {Component} from 'react';
import FrontFace from './frontFace';
import BackFace from './backFace';

class Contents extends Component {
    constructor(props) {
        super(props);      
    }

    componentWillMount() {     
        this.getQuestion();
    }


    getQuestion(){
        fetch('http://development.hestawork.com/kevin-poll/public//api/polls?email=undefined')
        .then(response => response.json())			    
        .then(res => {
            this.setState({
                question: res.poll
            });             
        });
    }

    render() {
        let content =   <div className="flip">
                            <div className="card">
                                <div className="face front">
                                    <FrontFace {...this.state} />
                                </div>
                                {/* <div className="face back">
                                    <FrontFace {...this.state}/>
                                </div> */}
                            </div>
                        </div>;
        return (content);
    }
}

export default Contents;