import React, { Component } from 'react';
import PropTypes from 'prop-types';

class OptionForm extends Component {
    constructor(props) {
        super(props);
        this.submitPoll = this.submitPoll.bind(this);
    }

    submitPoll(e, data) {
        fetch('http://development.hestawork.com/kevin-poll/public//api/polls?email=undefined')
            .then(response => response.json())
            .then(res => {
                this.setState({
                    question: res.poll
                });
            }).then(()=>{
                console.log(this.props);
            });
    }

    render() {  
        let options = this.props.question ? this.props.question.options : [];

        let optionStyle =   {
            display: 'block',
            cursor: 'pointer'
        };

        let option  =   <ul className="list-group">
                                {options.map((name, index) => {
                                    return  <li key={index} className="list-group-item" style={optionStyle} onClick={(e) => this.submitPoll(e, name)} >
                                            <lable style={optionStyle}>{name}</lable>
                                                <input type="hidden" name="" />
                                                <span className="stats"></span>
                                            </li>;
                                })}
                            </ul>;
        let optionForm  =   <form action="" id="target">
                                <input type="hidden" name="poll_id" />
                                {option}
                                <div className="text-center">
                                </div>
                            </form>;
        return (optionForm);
    }
}

export default OptionForm;