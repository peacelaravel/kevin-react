import React, { Component } from 'react';

class WhiteBox extends Component {
    constructor(props) {
        super(props);

        
    }

    render() {
        let question = this.props.question ? this.props.question.question : '';
        let category = this.props.question ? this.props.question.category[0].name : '';
        let icon     = this.props.question ? this.props.question.category[0].icon : '';
        let whiteBox =  <div className="white-box">
                            <img src={icon} width="100px" className="img-circle" alt="question-img"/>
                            <ul id="taglist">
                                <li>{category}</li>
                            </ul>
                            <h2><span>{question}</span></h2>
                        </div>;
        return (whiteBox);
    }
}

export default WhiteBox;