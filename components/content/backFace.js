import React, { Component } from 'react';

class BackFace extends Component {
    render() {
        let divStyle       =   {
            backgroundImage: 'url("https://image.freepik.com/free-psd/abstract-background-design_1297-87.jpg")',
            backgroundSize: 'cover'
        };
        let backface    =   <div className="back-img" style={divStyle}>
                                <div className="container">
                                    <div className="row">
                                        <div className="center-block content">
                                            <div className="col-md-4 center-block fn">
                                                <div className="question">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>;
        return (backface);
    }
}

export default BackFace;