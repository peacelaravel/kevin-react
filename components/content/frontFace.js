import React, { Component } from 'react';
import WhiteBox from './whiteBox';
import PollAlert from './pollAlert';
import OptionForm from './optionForm';

class FrontFace extends Component {
    constructor(props) {
        super(props);
        
    }
    render() {
        let divStyle       =   {
            backgroundImage: 'url("https://image.freepik.com/free-psd/abstract-background-design_1297-87.jpg")',
            backgroundSize: 'cover'
        };

        let frontface    =  <div className="back-img" style={divStyle}>
                                <div className="container">
                                    <div className="row">
                                        <div className="center-block content">
                                            <div className="col-md-4 center-block fn">
                                                <div className="question">
                                                    <WhiteBox {...this.props}/>
                                                    <h2>I DO</h2>
                                                    <PollAlert />
                                                    <OptionForm {...this.props}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>;
        return (frontface);
    }
}

export default FrontFace;